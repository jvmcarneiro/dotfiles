# Set modifier key
set $mod Mod4

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:SourceCodePro 10
for_window [class=".*"] title_format "%class - %title"

# Border and titlebar
default_border pixel 1
default_floating_border pixel 1
smart_borders on
client.focused #fdf6e3 #fdf6e3 #657b83 #fdf6e3
client.focused_inactive #eee8d5 #eee8d5 #93a1a1 #eee8d5
client.unfocused #eee8d5 #eee8d5 #93a1a1 #eee8d5 #eee8d5
client.urgent #cb4b16 #cb4b16 #fdf6e3 #cb4b16
client.background #fdf6e3

# Start polybar
exec_always --no-startup-id $HOME/.config/polybar/launch.sh

# Background
exec_always --no-startup-id feh --bg-fill "/home/jcarneiro/Pictures/wallpaper.jpg"
exec_always --no-startup-id dunst
exec_always --no-startup-id killall udiskie; udiskie -t2
exec_always --no-startup-id compton -b --shadow-exclude-reg 4000x20+0+0

# Toggle bar
bindsym $mod+g exec i3-msg bar mode hide
bindsym $mod+Shift+g exec i3-msg bar mode dock

# Volume control
bindsym XF86AudioRaiseVolume exec amixer -q -D pulse sset Master 5%+ && pkill -RTMIN+1 i3blocks
bindsym XF86AudioLowerVolume exec amixer -q -D pulse sset Master 5%- && pkill -RTMIN+1 i3blocks
bindsym XF86AudioMute exec amixer -q -D pulse sset Master toggle && pkill -RTMIN+1 i3blocks

# i3blocks scripts menu
bindsym Control+Mod1+Delete exec ~/.config/i3blocks/scripts/shutdown_menu

# start a terminal
bindsym $mod+Return exec termite

# kill focused window
bindsym $mod+Shift+q kill

# start dmenu (a program launcher)
bindsym $mod+d exec rofi -show run -lines 5 -width 30 -padding 1 -bw 0 -separator-style none

# change focus
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right
bindsym $mod+h focus left

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right
bindsym $mod+Shift+h move left

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+b split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
bindsym $mod+z focus child

# Move focused window to scratchpad
bindsym $mod+Shift+minus move scratchpad

# Show first scratchpad window
bindsym $mod+minus scratchpad show

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-msg exit"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym j resize grow height 10 px or 10 ppt
        bindsym k resize shrink height 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
#bar {
#  status_command i3blocks
#  position top
#  font pango:SourceCodePro, FontAwesome 10
#  tray_output primary
#  height 20
#  colors{
#    background #fdf6e3
#    statusline #657b83
#    separator #fdf6e3
#    focused_workspace #fdf6e3 #fdf6e3 #d33682
#    active_workspace #fdf6e3 #fdf6e3 #2aa198
#    inactive_workspace #fdf6e3 #fdf6e3 #93a1a1
#    urgent_workspace #fdf6e3 #fdf6e3 #cb4b16
#  }
#}
