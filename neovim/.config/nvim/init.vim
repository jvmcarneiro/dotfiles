" Enable lxterminal support
let $NVIM_TUI_ENABLE_CURSOR_SHAPE = 0

" Auto indentation per file type
filetype plugin indent on

" Remember last cursor position
augroup lastposition
  autocmd BufReadPost * if @% !~# '\.git[\/\\]COMMIT_EDITMSG$' &&
        \ line("'\"") > 1 && line("'\"") <= line("$") | 
        \exe "normal! g`\"" | endif 
augroup END

" Set solarized theme
syntax enable
set background=dark
colorscheme solarized

" Auto indentation per file type
filetype plugin indent on

" Set text width for automatic word wrap
set textwidth=80

" Save undo history in persistent files
set undofile

" Yank and paste from system clipboard
set clipboard+=unnamedplus

" Open tree navigation
nnoremap <C-n> :NERDTreeToggle<CR>

" Open tagbar navigation
nnoremap <C-m> :TagbarToggle<CR>

" Treat display lines as normal
noremap <silent> k gk
noremap <silent> j gj
noremap <silent> 0 g0
noremap <silent> $ g$

" Maps enter to clear search highlighting
nnoremap <C-l> :noh<CR><CR>

" Set latex flavor
let g:tex_flavor = 'latex'

" Lightline config to include syntastic flags
let g:lightline = {
      \ 'colorscheme': 'solarized',
      \ 'active': {
      \   'right': [ [ 'syntastic', 'lineinfo' ],
      \              [ 'percent' ],
      \              [ 'fileformat', 'fileencoding', 'filetype' ] ]
      \ },
      \ 'component_expand': {
      \   'syntastic': 'SyntasticStatuslineFlag',
      \ },
      \ 'component_type': {
      \   'syntastic': 'error',
      \ }
      \ }
function! SyntasticCheckHook(errors)
  call lightline#update()
endfunction

" Syntastic config for multiple filetypes
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_c_checkers = ['cppcheck', 'gcc', 'make']
let g:syntastic_c_compiler_options = '-Wall -Wextra -Werror -std=gnu11'
let g:syntastic_cpp_checkers = ['cpplint', 'gcc', 'cppcheck']
let g:syntastic_cpp_cpplint_exec = 'cpplint'
let g:syntastic_cpp_cpplint_args = '--verbose=0 --filter=-legal/copyright'
let g:syntastic_cpp_compiler_options = '-Wall -Wextra -Werror -std=c++17'
let g:syntastic_markdown_checkers = ['mdl']
let g:syntastic_markdown_mdl_exec = 'markdownlint'
let g:syntastic_markdown_mdl_args = ''
let g:syntastic_tex_checkers = ['chktex', 'lacheck']
let g:syntastic_vim_checkers = ['vint']

" Limits syntastic window size
function! SyntasticCheckHook(errors)
    if !empty(a:errors)
        let g:syntastic_loc_list_height = min([len(a:errors), 10])
    endif
endfunction

" Activate rainbow braces and parenthesis
let g:rainbow_active = 1
let g:rainbow_conf = {
\	'guifgs': ['darkred', 'red', 'brown', 'darkgreen', 'darkcyan', 'darkblue', 'magenta', 'darkmagenta'],
\	'ctermfgs': ['darkred', 'red', 'brown', 'darkgreen', 'darkcyan', 'darkblue', 'magenta', 'darkmagenta']
\}

" Specify a directory for plugins
call plug#begin('~/.local/share/nvim/plugged')

" Breezy color theme
Plug 'altercation/vim-colors-solarized'
" Color names and hex codes
Plug 'chrisbra/Colorizer'
" Tree folder navigation panel
Plug 'scrooloose/nerdtree'
" Nerdtree support for git flags
Plug 'Xuyuanp/nerdtree-git-plugin'
" Tab completion
Plug 'ervandew/supertab'
" Syntax checker for multiple languages
Plug 'vim-syntastic/syntastic'
" Tex live preview
Plug 'donRaphaco/neotex', { 'for': 'tex' }
" Fancy status line
Plug 'itchyny/lightline.vim'
" Indentation detection
Plug 'tpope/vim-sleuth'
" Colored limiters
Plug 'luochen1990/rainbow'
" Bindings for surrounded stuff
Plug 'tpope/vim-surround'
" Automatic tag maintenance
Plug 'ludovicchabant/vim-gutentags'
" Extended repeat
Plug 'tpope/vim-repeat'
" Extra mappings
Plug 'tpope/vim-unimpaired'
" Tag sidebar
Plug 'majutsushi/tagbar'
" Git integration
Plug 'tpope/vim-fugitive'

" Initialize plugin system
call plug#end()
