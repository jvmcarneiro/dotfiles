[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=Breeze
Font=Ubuntu Mono,11,-1,0,50,0,0,0,0,0,Regular
UseFontLineChararacters=true

[General]
Environment=TERM=xterm-256color,COLORTERM=truecolor
Name=Profile 1
Parent=FALLBACK/

[Keyboard]
KeyBindings=default

[Scrolling]
HistoryMode=2
HistorySize=100000
ScrollBarPosition=2
